/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase1;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class WFileToSend {

    public static long nextID = 0;
    private long id, fileOffset;
    private int totalParts;
    private int currentPart;
    private int relayID;
    private ArrayList<Point> filesXored = new ArrayList<>();
    private File file;
    private ArrayList<Integer> J = new ArrayList<>();

    public static long getNextID() {
        return nextID;
    }

    public static void setNextID(long nextID) {
        WFileToSend.nextID = nextID;
    }

    public long getFileOffset() {
        return fileOffset;
    }

    public void setFileOffset(long fileOffset) {
        this.fileOffset = fileOffset;
    }

    public WFileToSend() {
        id = nextID++;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Integer> getJ() {
        return J;
    }

    public void setJ(ArrayList<Integer> J) {
        this.J = J;
    }

    public int getRelayID() {
        return relayID;
    }

    public void setRelayID(int relayID) {
        this.relayID = relayID;
    }

    public int getTotalParts() {
        return totalParts;
    }

    public void setTotalParts(int totalParts) {
        this.totalParts = totalParts;
    }

    public int getCurrentPart() {
        return currentPart;
    }

    public void setCurrentPart(int currentPart) {
        this.currentPart = currentPart;
    }

    public ArrayList<Point> getFilesXored() {
        return filesXored;
    }

    public void setFilesXored(ArrayList<Point> filesXored) {
        this.filesXored = filesXored;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
