/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase2;

import com.tfg.server.Constants;
import com.tfg.server.delivery.Phase1KnownData;
import com.tfg.server.delivery.algorithm.phase1.GeneralAlgorithmPhase1.GeneralAlgorithmPhase1Result;
import com.tfg.server.delivery.algorithm.phase1.WFileToSend;
import com.tfg.server.delivery.algorithm.phase1.W_File;
import com.tfg.server.topology.Relay;
import com.tfg.server.topology.User;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author eryalus
 */
public class GeneralAlgorithmPhase2 {

    public ArrayList<PToSend> run(GeneralAlgorithmPhase1Result Data, HashMap<W_File, Phase1KnownData> knownData) throws FileNotFoundException, IOException {

        ArrayList<P> Ps = new ArrayList<>();
        for (Relay r : Constants.TOPOLOGY.getRelays()) {
            for (Integer u : Constants.TOPOLOGY.getUsers().keySet()) {
                for (Integer u2 : Constants.TOPOLOGY.getUsers().keySet()) {
                    if (((int) u) != ((int) u2)) {
                        P p = new P();
                        p.toRelay = r.getId();
                        p.neededBy = u;
                        p.knownBy = u2;
                        Ps.add(p);
                    }
                }
            }
        }
        ArrayList<SubP> subPs = new ArrayList<>();
        int counter = 0, pieceCounter = 0;
        for (W_File file : Data.getWInternalFiles()) {
            /**
             * find users that cannot recover the file. Select the users
             * connected to the relay that have received the file and are in the
             * J vector. This users can recover the file so -> the users in J
             * that are not connected to relay h cannot recover the file. Let's
             * find those users:
             */
            ArrayList<Integer> users = new ArrayList<>();
            Relay relay = null;
            for (Relay r : Constants.TOPOLOGY.getRelays()) {
                if (r.getId() == file.getRelayID()) {
                    relay = r;
                }
            }
            if (relay == null) {
                throw new NullPointerException();
            }
            Set<Integer> connectedUsers = relay.getConnectedUsers().keySet();
            for (Integer JValue : file.getJ()) {
                boolean contains = false;
                for (Integer connectedUser : connectedUsers) {
                    if (((int) connectedUser) == ((int) JValue)) {
                        contains = true;
                        break;
                    }
                }
                if (!contains) {
                    users.add(JValue);
                }
            }
            /**
             * "users" contains the users' ids that cannot recover the file.
             * It's an arraylist but it will just contains 0 or 1 element.
             */

            /**
             * Now, W subfile must be partitioned into t equal-length parts.
             * Generalizing, it must be partitiones into x non-overlapping
             * equal-length parts being x = number of relays connected to the
             * users that cannont recover the file
             */
            if (users.isEmpty()) {
                Constants.LOG.info(file.getJ().toString() + " -> All can recover the files");
                counter++;
                continue;
            }
            Constants.LOG.info(file.getJ().toString() + " -> " + users.toString() + " cannot recover the files");
            for (int us : users) {
                User user = Constants.TOPOLOGY.getUsers().get(us);

                if (user == null) {
                    Constants.LOG.error("User not found.");
                    counter++;
                    continue;
                }
                int numberOfParts = user.getConnectedRelays().size();
                long length = file.getFile().length();
                double blockSizeD = ((double) length) / numberOfParts;
                double rest = blockSizeD - Math.floor(blockSizeD);
                long blockSize = length / numberOfParts;
                if (rest != 0) {
                    blockSize++;
                }
                Constants.LOG.info("User found. Divide into " + numberOfParts + " of " + blockSize + "B (Original " + length + "B)");
                long byteCounter = 0, totalBytes = 0;
                DataInputStream in = new DataInputStream(new FileInputStream(file.getFile().getAbsoluteFile()));
                byte[] buffer = new byte[1];
                ArrayList<File> parts = new ArrayList<>();
                File tempFile = File.createTempFile("tempPhase2", ".tmp");
                DataOutputStream out = new DataOutputStream(new FileOutputStream(tempFile));
                Constants.LOG.info("Writing file " + tempFile.getAbsolutePath() + "...");
                int partCounter = 0;
                while (in.read(buffer) != -1) {
                    out.write(buffer);
                    byteCounter++;
                    totalBytes++;
                    if (byteCounter >= blockSize && partCounter < numberOfParts) {
                        SubP subP = new SubP();
                        subP.file = tempFile;
                        subP.relay = user.getConnectedRelays().get(partCounter).getId();
                        Constants.LOG.info("To Relay: " + subP.relay);
                        subP.relayPart = partCounter;
                        subP.relayCount = numberOfParts;//user.getConnectedRelays().size();
                        subP.wFile = file;
                        subP.user = user;
                        subP.offset = blockSize * partCounter;
                        subP.wFileToSend = Data.getWFilesToSend().get(counter);
                        /**
                         * Found users connected to the same relay that knows
                         * the file.
                         */
                        ArrayList<Integer> knows = knownData.get(file).getUsersThatKnow();
                        for (P p : Ps) {
                            if (p.toRelay == subP.relay && p.neededBy == subP.user.getId()) {
                                for (Integer u : knows) {
                                    if (((int) u) == p.knownBy) {
                                        p.subPs.add(subP);
                                    }
                                }
                            }
                        }

                        subPs.add(subP);
                        out.close();
                        byteCounter = 0;
                        parts.add(tempFile);
                        tempFile = File.createTempFile("tempPhase2", ".tmp");
                        Constants.LOG.info("Writing file " + tempFile.getAbsolutePath() + "...");
                        out = new DataOutputStream(new FileOutputStream(tempFile));
                        partCounter++;
                    }
                }
                Constants.LOG.info("Writed bytes: " + totalBytes + " " + byteCounter);
                if (byteCounter != 0) {
                    SubP subP = new SubP();
                    subP.file = tempFile;
                    subP.relay = user.getConnectedRelays().get(partCounter).getId();
                    Constants.LOG.info("To Relay: " + subP.relay);
                    subP.relayCount = user.getConnectedRelays().size();
                    subP.wFile = file;
                    subP.relayPart = partCounter;
                    subP.user = user;
                    subP.offset = blockSize * partCounter;
                    subP.wFileToSend = Data.getWFilesToSend().get(counter);
                    /**
                     * Found users connected to the same relay that knows the
                     * file.
                     */
                    ArrayList<Integer> knows = knownData.get(file).getUsersThatKnow();
                    for (P p : Ps) {
                        if (p.toRelay == subP.relay && p.neededBy == subP.user.getId()) {
                            for (Integer u : knows) {
                                if (((int) u) == p.knownBy) {
                                    p.subPs.add(subP);
                                }
                            }
                        }
                    }

                    subPs.add(subP);
                    out.close();
                    parts.add(tempFile);
                }
                out.close();
            }
            /**
             * Now the file is divided in numberOfParts files. (approx
             * equal-size). The files are saved on the ArrayList
             */
            counter++;
        }
        /*
        for (SubP subP : subPs) {
            ArrayList<Integer> knows = new ArrayList<>();
            int rel = subP.wFile.getRelayID();
            for (Relay r : Constants.TOPOLOGY.getRelays()) {
                if (r.getId() == rel) {
                    for (Integer u : r.getConnectedUsers().keySet()) {
                        knows.add(u);
                    }
                    break;
                }
            }
            //System.out.println(subP.wFile.getJ().toString() + " " + subP.wFileToSend.getRelayID() + " " + subP.relay + " , "+knows.toString());
            for (P p : Ps) {
                if (p.toRelay == subP.relay && p.neededBy == subP.user.getId()) {
                    for (Integer u : knows) {
                        if (((int) u) == p.knownBy) {
                            p.subPs.add(subP);
                        }
                    }
                }
            }
        }*/

//        for (Relay r : Constants.TOPOLOGY.getRelays()) {
//            for (Integer u : Constants.TOPOLOGY.getUsers().keySet()) {
//                for (Integer u2 : Constants.TOPOLOGY.getUsers().keySet()) {
//                    if (((int) u) != ((int) u2)) {
//                        P p = new P();
//                        p.toRelay = r.getId();
//                        p.knownBy = u;
//                        p.neededBy = u2;
//                        Ps.add(p);
//                    }
//                }
//            }
//        }
        /**
         * delete unnecessary P on Ps.
         */
        ArrayList<P> toDelete = new ArrayList<>();
        for (P p : Ps) {
            if (p.subPs.isEmpty()) {
                toDelete.add(p);
            }
        }
        Ps.removeAll(toDelete);
        for (P p : Ps) {
            if (p.subPs.isEmpty()) {
                continue;
            }
            System.out.print(p.toRelay + " " + p.neededBy + " " + p.knownBy + " {");
            for (SubP sp : p.subPs) {
                System.out.print(sp.wFileToSend.getId() + " " + sp.wFile.getJ().toString() + " " + sp.wFile.getRelayID() + " " + sp.relay + ",");
            }
            System.out.println("}");
        }
        /*
        for (P p : Ps) {
            Constants.LOG.info("Writing " + p.toRelay + " " + p.neededBy + " " + p.knownBy + "...");
            p.file = File.createTempFile("noXoredP_", ".tmp");
            DataOutputStream out = new DataOutputStream(new FileOutputStream(p.file));
            for (SubP s : p.subPs) {
                System.out.println("SubparSizes: "+s.file.length());
                DataInputStream in = new DataInputStream(new FileInputStream(s.file));
                byte[] buffer = new byte[1024];
                int readed;
                while ((readed = in.read(buffer)) != -1) {
                    out.write(buffer, 0, readed);
                }
                in.close();
            }
            out.close();
        }
         */
        ArrayList<PToSend> PsToSend = new ArrayList<>();
        for (P p : Ps) {
            for (P p2 : Ps) {
                if (p2.toRelay == p.toRelay && p2.knownBy == p.neededBy && p2.neededBy == p.knownBy) {
                    p.done = true;
                    p2.done = true;
                    Constants.LOG.info("Calculating (" + p.toRelay + " " + p.neededBy + " " + p.knownBy + ") xor (" + p2.toRelay + " " + p2.neededBy + " " + p2.knownBy + ")");
                    //System.out.println("Sizes: " + p.file.length() + " " + p2.file.length());
                    int max = Math.max(p.subPs.size(), p2.subPs.size());
                    for (int i = 0; i < p.subPs.size(); i++) {
                        SubP subp1;
                        if (i < p.subPs.size()) {
                            subp1 = p.subPs.get(i);
                        } else {
                            subp1 = p.subPs.get(0);
                        }
                        SubP subp2;
                        if (i < p2.subPs.size()) {
                            subp2 = p2.subPs.get(i);
                        } else {
                            subp2 = p2.subPs.get(0);
                        }
                        DataInputStream in1;//= new DataInputStream(new FileInputStream(subp1.file));
                        DataInputStream in2;//= new DataInputStream(new FileInputStream(subp2.file));
                        Constants.LOG.error("Possible error: " + subp1.file.length() + " " + subp2.file.length());
                        File tfile = File.createTempFile("finalPhase2_", ".tmp");
                        DataOutputStream out = new DataOutputStream(new FileOutputStream(tfile));
                        long maxFilesize = Math.max(subp1.file.length(), subp2.file.length());
                        long minFilesize = Math.min(subp1.file.length(), subp2.file.length());

                        if (subp1.file.length() > subp2.file.length()) {
                            in1 = new DataInputStream(new FileInputStream(subp1.file));
                            in2 = new DataInputStream(new FileInputStream(subp2.file));
                        } else {
                            in1 = new DataInputStream(new FileInputStream(subp2.file));
                            in2 = new DataInputStream(new FileInputStream(subp1.file));
                        }
                        byte[] buffer1 = new byte[1];
                        byte[] buffer2 = new byte[1];
                        int readed;
                        for (long r = 0; r < maxFilesize; r++) {
                            in1.read(buffer1);
                            if (r < minFilesize) {
                                in2.read(buffer2);
                                out.write(new byte[]{xor(buffer1[0], buffer2[0])});
                            } else {
                                out.write(buffer1);
                            }
                        }
                        /*
                        while ((readed = in1.read(buffer1)) != -1) {
                            in2.read(buffer2);
                            for (int j = 0; j < readed; j++) {
                                out.write(new byte[]{xor(buffer1[j], buffer2[j])});
                            }
                        }*/
                        out.close();
                        in1.close();
                        in2.close();
                        Constants.LOG.info("File Size: " + tfile.length());
                        //know i have the xor of both subP
                        PToSend pToSend = new PToSend();
                        pToSend.setUser1(p.knownBy);
                        pToSend.setUser2(p.neededBy);
                        pToSend.setFile(tfile);
                        pToSend.toRelay = p.toRelay;
                        ArrayList<WFile> list = new ArrayList<>();
                        WFile wf = new WFile();
                        wf.setPartID(subp1.wFileToSend.getId());
                        wf.setTotalRelay(subp1.relayCount);
                        wf.setToRelayCounter(subp1.relayPart);
                        wf.setToRelay(subp1.relay);
                        wf.setFileLength(subp1.file.length());
                        wf.setOffset(subp1.offset);
                        wf.setInternal_offset(subp1.wFile.getOffset());
                        wf.setInternal_currentPart(subp1.wFileToSend.getCurrentPart());
                        wf.setInternal_filesXored(subp1.wFileToSend.getFilesXored());
                        wf.setInternal_totalParts(subp1.wFileToSend.getTotalParts());
                        wf.setInternal_fileSize(subp1.wFileToSend.getFile().length());
                        list.add(wf);
                        wf = new WFile();
                        wf.setPartID(subp2.wFileToSend.getId());
                        wf.setToRelayCounter(subp2.relayPart);
                        wf.setTotalRelay(subp2.relayCount);
                        wf.setToRelay(subp2.relay);
                        wf.setFileLength(subp2.file.length());
                        wf.setOffset(subp2.offset);
                        wf.setInternal_offset(subp2.wFile.getOffset());
                        wf.setInternal_currentPart(subp2.wFileToSend.getCurrentPart());
                        wf.setInternal_filesXored(subp2.wFileToSend.getFilesXored());
                        wf.setInternal_totalParts(subp2.wFileToSend.getTotalParts());
                        wf.setInternal_fileSize(subp2.wFileToSend.getFile().length());
                        list.add(wf);
                        pToSend.subParts = list;
                        PsToSend.add(pToSend);
                    }

                }
            }
        }
        for (P p : Ps) {
            if (!p.done) {
                Constants.LOG.error("Error xoring \"" + p.toRelay + " " + p.neededBy + " " + p.knownBy + "\"");
            }
        }
        return PsToSend;

    }

    private byte xor(byte byte1, byte byte2) {
        return (byte) ((int) byte1 ^ (int) byte2);
    }

    private class P {

        boolean done = false;
        int toRelay, neededBy, knownBy;
        File file;
        ArrayList<SubP> subPs = new ArrayList<>();
    }

    private class SubP {

        User user;
        WFileToSend wFileToSend;
        W_File wFile;
        File file;
        int relay;
        int relayPart;
        int relayCount;
        long offset;
    }

}
