/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase1;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class W_File {

    int S, relayID;
    ArrayList<Integer> d = new ArrayList<>(), J = new ArrayList<>();
    private File file;
    private long offset;

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "S:" + S + "\nrelay:" + relayID + "\nD: " + d.toString() + "\nJ:" + J.toString() + "\nFile: " + file.getAbsolutePath();
    }

    public int getS() {
        return S;
    }

    public void setS(int S) {
        this.S = S;
    }

    public int getRelayID() {
        return relayID;
    }

    public void setRelayID(int relayID) {
        this.relayID = relayID;
    }

    public ArrayList<Integer> getD() {
        return d;
    }

    public void setD(ArrayList<Integer> d) {
        this.d = d;
    }

    public ArrayList<Integer> getJ() {
        return J;
    }

    public void setJ(ArrayList<Integer> J) {
        this.J = J;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
