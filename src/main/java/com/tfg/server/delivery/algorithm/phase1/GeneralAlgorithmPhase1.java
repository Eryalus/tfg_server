/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase1;

import com.tfg.server.Constants;
import com.tfg.server.fileProcessing.ProcessedFile;
import com.tfg.server.fileProcessing.Subfile;
import com.tfg.server.topology.Relay;
import com.tfg.server.topology.User;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class GeneralAlgorithmPhase1 {

    public class GeneralAlgorithmPhase1Result {

        private HashMap<Integer, ArrayList<W_File>> WInternalForUser = new HashMap<>();
        private HashMap<Integer, ArrayList<WFileToSend>> WFilesToSendForUser = new HashMap<>();
        private ArrayList<W_File> WInternalFiles;
        private ArrayList<WFileToSend> WFilesToSend;

        public GeneralAlgorithmPhase1Result(ArrayList<W_File> WInternalFiles, ArrayList<WFileToSend> WFilesToSend) {
            this.WInternalFiles = WInternalFiles;
            this.WFilesToSend = WFilesToSend;
        }

        public ArrayList<W_File> getWInternalFiles() {
            return WInternalFiles;
        }

        public ArrayList<WFileToSend> getWFilesToSend() {
            return WFilesToSend;
        }

    }

    public GeneralAlgorithmPhase1Result run(ArrayList<ProcessedFile> ProcessedFiles, HashMap<Integer, Integer> D_VECTOR) throws IOException {
        ArrayList<W_File> WFiles = new ArrayList<>();
        ArrayList<WFileToSend> WFilesToSend = new ArrayList<>();
        HashMap<Integer, ArrayList<W_File>> WInternalForUser = new HashMap<>();
        HashMap<Integer, ArrayList<WFileToSend>> WFilesToSendForUser = new HashMap<>();
        for (int u : Constants.TOPOLOGY.getUsers().keySet()) {
            WInternalForUser.put(u, new ArrayList<>());
            WFilesToSendForUser.put(u, new ArrayList<>());
        }
        ArrayList<Integer> D = new ArrayList<>();
        for (Integer d : D_VECTOR.values()) {
            D.add(d);
        }
        Constants.LOG.info("Calculating S vector...");
        ArrayList<ArrayList<Integer>> J_VECTOR = Constants.J_COMBINATIONS;
        ArrayList<ArrayList<Integer>> S_VECTOR = new ArrayList<>();
        J_VECTOR.forEach((J) -> {
            S_VECTOR.add(getSforJ(J));
        });
        String temp = "";
        for (ArrayList<Integer> S : S_VECTOR) {
            for (Integer S_e : S) {
                temp += S_e;
            }
            temp += " ";
        }
        Constants.LOG.info("S vector calculated. " + temp.trim());
        Constants.LOG.info("Creating W...");
        int counter = 0;
        new File(Constants.W_FILES_PATH).mkdirs();
        for (ArrayList<Integer> J : J_VECTOR) {
            double wsizeD = ((double) Constants.SUBFILE_SIZE) / S_VECTOR.get(counter).size();
            double rest = wsizeD - Math.floor(wsizeD);
            long WSize = Constants.SUBFILE_SIZE / S_VECTOR.get(counter).size();
            if (rest != 0) {
                WSize = WSize + 1;
            }
            System.out.println("WSize: " + WSize + " SSize:" + S_VECTOR.get(counter).size() + " SubfileSize:" + Constants.SUBFILE_SIZE);
            //generate W file for given J
            Constants.LOG.info("J:" + J.toString());
            File fileToWrite = File.createTempFile("subW", ".tmp");
            ArrayList<File> filesToXor = new ArrayList<>();
            ArrayList<Point> subfileToXor = new ArrayList<>();
            for (Integer userID : J) {
                Constants.LOG.info("userID:" + userID);
                int requestedFile = D_VECTOR.get(userID);
                ArrayList<Subfile> subfiles = new ArrayList<>();
                ProcessedFile pFile = null;
                for (ProcessedFile pfile : ProcessedFiles) {
                    if (pfile.getId() == requestedFile) {
                        subfiles = pfile.getSubfiles();
                        pFile = pfile;
                        break;
                    }
                }
                String users_s = "";
                for (Subfile s : subfiles) {
                    boolean valid = true;
                    for (User users : s.getUsers()) {
                        boolean contains = false;
                        for (Integer id : J) {
                            if (((int) id) != userID) {
                                if (users.getId() == ((int) id)) {
                                    contains = true;
                                    break;
                                }
                            }
                        }
                        if (!contains) {
                            valid = false;
                            break;
                        }
                    }
                    if (valid) {
                        for (User u : s.getUsers()) {
                            users_s += u.getId();
                        }
                        filesToXor.add(s.getFile());
                        subfileToXor.add(new Point(pFile.getId(), Integer.parseInt(s.getFile().getName())));
                        Constants.LOG.info("Selecting W" + requestedFile + "," + users_s);
                    }
                }

            }
            DataInputStream[] readers = new DataInputStream[filesToXor.size()];
            DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(fileToWrite));
            for (int i = 0; i < readers.length; i++) {
                readers[i] = new DataInputStream(new FileInputStream(filesToXor.get(i)));
            }
            for (long i = 0; i < Constants.SUBFILE_SIZE; i++) {
                Byte result = null;
                for (int j = 0; j < readers.length; j++) {
                    if (j == 0) {
                        result = readers[j].readByte();
                    } else {
                        result = xor(result, readers[j].readByte());
                    }
                }
                outputFile.write(result);
            }
            outputFile.close();
            System.out.println(fileToWrite.getAbsolutePath());
            for (int i = 0; i < readers.length; i++) {
                readers[i].close();
            }
            //W file is on fileToWrite
            //W must be divided in |S| files of WSize size
            Constants.LOG.info("Starting partition W on " + S_VECTOR.get(counter).size() + " files...");
            long writedBytes = 0L;
            int partIntex = 0;
            DataInputStream in = new DataInputStream(new FileInputStream(fileToWrite));
            File outFile = File.createTempFile("subSubW", ".tmp");
            DataOutputStream out = new DataOutputStream(new FileOutputStream(outFile));
            byte[] read = new byte[1];
            while (in.read(read) != -1) {
                out.write(read);
                if (++writedBytes % WSize == 0) {
                    out.close();
                    // new things to add goes here
                    Constants.LOG.info("WRITEEEEEEEEED: " + writedBytes);
                    Constants.LOG.info("Offset: " + (writedBytes - outFile.length()));
                    W_File w = new W_File();
                    w.setFile(outFile);
                    w.setOffset(writedBytes - outFile.length());
                    w.setJ(J);
                    w.setRelayID(S_VECTOR.get(counter).get(partIntex));
                    w.setS(S_VECTOR.get(counter).size());
                    w.setD(D);
                    WFiles.add(w);
                    WFileToSend wToSend = new WFileToSend();
                    wToSend.setCurrentPart(partIntex);
                    wToSend.setTotalParts(S_VECTOR.get(counter).size());
                    wToSend.setFile(outFile);
                    wToSend.setFilesXored(subfileToXor);
                    wToSend.setRelayID(S_VECTOR.get(counter).get(partIntex));
                    wToSend.setJ(J);
                    wToSend.setFileOffset(writedBytes - outFile.length());
                    WFilesToSend.add(wToSend);
                    for (Relay r : Constants.TOPOLOGY.getRelays()) {
                        if (r.getId() == wToSend.getRelayID()) {
                            for (Integer u : r.getConnectedUsers().keySet()) {
                                WFilesToSendForUser.get(u).add(wToSend);
                                WInternalForUser.get(u).add(w);
                            }
                        }
                    }
                    Constants.LOG.info("File " + partIntex + " saved on " + outFile.getAbsolutePath());
                    partIntex++;
                    outFile = File.createTempFile("subSubW", ".tmp");
                    out = new DataOutputStream(new FileOutputStream(outFile));
                }
            }
            if (writedBytes % WSize != 0) {
                out.close();
                // new things to add goes here
                Constants.LOG.info("WRITEEEEEEEEED: " + writedBytes);
                W_File w = new W_File();
                w.setOffset(writedBytes - outFile.length());
                w.setFile(outFile);
                w.setJ(J);
                w.setRelayID(S_VECTOR.get(counter).get(partIntex));
                w.setS(S_VECTOR.get(counter).size());
                w.setD(D);
                WFiles.add(w);
                WFileToSend wToSend = new WFileToSend();
                wToSend.setCurrentPart(partIntex);
                wToSend.setTotalParts(S_VECTOR.get(counter).size());
                wToSend.setFile(outFile);
                wToSend.setFilesXored(subfileToXor);
                wToSend.setRelayID(S_VECTOR.get(counter).get(partIntex));
                wToSend.setJ(J);
                wToSend.setFileOffset(writedBytes - outFile.length());
                WFilesToSend.add(wToSend);
                for (Relay r : Constants.TOPOLOGY.getRelays()) {
                    if (r.getId() == wToSend.getRelayID()) {
                        for (Integer u : r.getConnectedUsers().keySet()) {
                            WFilesToSendForUser.get(u).add(wToSend);
                            WInternalForUser.get(u).add(w);
                        }
                    }
                }
                Constants.LOG.info("File " + partIntex + " saved on " + outFile.getAbsolutePath());
                partIntex++;
                outFile = File.createTempFile("subSubW", ".tmp");
                out = new DataOutputStream(new FileOutputStream(outFile));
            }
            out.close();
            in.close();
            counter++;
        }
        return new GeneralAlgorithmPhase1Result(WFiles, WFilesToSend);
    }

    private byte xor(byte byte1, byte byte2) {
        return (byte) ((int) byte1 ^ (int) byte2);
    }

    private ArrayList<Integer> getSforJ(ArrayList<Integer> J) {
        //S -> set of relays each of which is connected to the largest number of users in J
        int[] connectedUsersCounters = new int[Constants.TOPOLOGY.getRelays().size()];
        for (int i = 0; i < connectedUsersCounters.length; i++) {
            connectedUsersCounters[i] = 0;
        }
        int counter = 0;
        for (Relay r : Constants.TOPOLOGY.getRelays()) {
            for (Integer ConnectedUser : r.getConnectedUsers().keySet()) {
                boolean contains = false;
                for (Integer j : J) {
                    if (((int) ConnectedUser) == ((int) j)) {
                        contains = true;
                        break;
                    }
                }
                if (contains) {
                    connectedUsersCounters[counter] = connectedUsersCounters[counter] + 1;
                }
            }
            counter++;
        }
        int maxCounter = 0;
        for (int i = 0; i < connectedUsersCounters.length; i++) {
            if (maxCounter < connectedUsersCounters[i]) {
                maxCounter = connectedUsersCounters[i];
            }
        }
        ArrayList<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < connectedUsersCounters.length; i++) {
            if (maxCounter == connectedUsersCounters[i]) {
                indexes.add(i + 1);
            }
        }
        //indexes contains the relays with the largest number of users in J (S)
        return indexes;
    }
}
