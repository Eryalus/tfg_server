/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.server.Constants;
import com.tfg.server.delivery.Phase1KnownData;
import com.tfg.server.delivery.algorithm.phase1.GeneralAlgorithmPhase1.GeneralAlgorithmPhase1Result;
import com.tfg.server.topology.Relay;
import com.tfg.server.utils.SocketWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class Phase1Sender {

    public HashMap<W_File, Phase1KnownData> send(GeneralAlgorithmPhase1Result files) throws IOException {
        SocketWriter writer = Constants.WRITER;
        HashMap<W_File, Phase1KnownData> knownFiles = new HashMap<>();
        int counter = 0;
        for (WFileToSend file : files.getWFilesToSend()) {
            writer.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE);
            writer.writeInt32(file.getRelayID());
            writer.writeLong(file.getId());
            writer.writeInt32(file.getTotalParts());
            writer.writeInt32(file.getCurrentPart());
            writer.writeLong(file.getFileOffset());
            String json = new ObjectMapper().writeValueAsString(file.getFilesXored());
            writer.writeString(json);
            json = new ObjectMapper().writeValueAsString(file.getJ());
            writer.writeString(json);
            writer.writeFile(file.getFile());
            for (Relay r : Constants.TOPOLOGY.getRelays()) {
                if (r.getId() == file.getRelayID()) {
                    ArrayList<Integer> users = new ArrayList<>();
                    for (Integer u : r.getConnectedUsers().keySet()) {
                        users.add(u);
                    }
                    Phase1KnownData p1kd = new Phase1KnownData();
                    p1kd.setUsersThatKnow(users);
                    p1kd.setwFile(files.getWInternalFiles().get(counter));
                    p1kd.setwFileToSend(file);
                    knownFiles.put(files.getWInternalFiles().get(counter++), p1kd);
                }
            }
        }
        writer.flush();
        return knownFiles;
    }
}
