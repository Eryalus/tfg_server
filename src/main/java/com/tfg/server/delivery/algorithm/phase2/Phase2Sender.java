/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.server.Constants;
import com.tfg.server.utils.SocketWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class Phase2Sender {

    public void send(ArrayList<PToSend> files) throws IOException {
        SocketWriter writer = Constants.WRITER;
        for (PToSend file : files) {

            writer.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PHASE_2_MESSAGE);
            writer.writeInt32(file.getToRelay());
            MessageToSend m = new MessageToSend();
            m.setMessage(file.getSubParts());
            m.setUser1(file.getUser1());
            m.setUser2(file.getUser2());
            String json = new ObjectMapper().writeValueAsString(m);
            writer.writeString(json);
            writer.writeFile(file.getFile());
            /*
            writer.writeInt32(file.getRelayID());
            writer.writeInt32(file.getTotalParts());
            writer.writeInt32(file.getCurrentPart());
            String json = new ObjectMapper().writeValueAsString(file.getFilesXored());
            writer.writeString(json);
            writer.writeFile(file.getFile());
             */
        }
        writer.flush();
    }
}
