/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery.algorithm.phase2;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class PToSend {

    private File file;
    int toRelay;
    ArrayList<WFile> subParts = new ArrayList<>();
    int user1, user2;

    public int getUser1() {
        return user1;
    }

    public void setUser1(int user1) {
        this.user1 = user1;
    }

    public int getUser2() {
        return user2;
    }

    public void setUser2(int user2) {
        this.user2 = user2;
    }

    public int getToRelay() {
        return toRelay;
    }

    public void setToRelay(int toRelay) {
        this.toRelay = toRelay;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<WFile> getSubParts() {
        return subParts;
    }

    public void setSubParts(ArrayList<WFile> subParts) {
        this.subParts = subParts;
    }

}
