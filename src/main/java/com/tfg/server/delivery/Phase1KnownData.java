/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery;

import com.tfg.server.delivery.algorithm.phase1.WFileToSend;
import com.tfg.server.delivery.algorithm.phase1.W_File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class Phase1KnownData {

    WFileToSend wFileToSend;
    W_File wFile;
    ArrayList<Integer> usersThatKnow;

    public ArrayList<Integer> getUsersThatKnow() {
        return usersThatKnow;
    }

    public void setUsersThatKnow(ArrayList<Integer> usersThatKnow) {
        this.usersThatKnow = usersThatKnow;
    }

    public WFileToSend getwFileToSend() {
        return wFileToSend;
    }

    public void setwFileToSend(WFileToSend wFileToSend) {
        this.wFileToSend = wFileToSend;
    }

    public W_File getwFile() {
        return wFile;
    }

    public void setwFile(W_File wFile) {
        this.wFile = wFile;
    }

}
