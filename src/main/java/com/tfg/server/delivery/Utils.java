/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.delivery;

import com.tfg.server.Constants;
import com.tfg.server.fileProcessing.Subfile;
import com.tfg.server.topology.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.apache.commons.math3.special.Gamma;

/**
 *
 * @author eryalus
 */
public class Utils {

    public void calculateJ() {
        ArrayList<ArrayList<Integer>> combinations = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();
        Collection<User> usersColl = Constants.TOPOLOGY.getUsers().values();
        usersColl.forEach((u) -> {
            users.add(u);
        });
        Collections.sort(users);
        int totalCombinations = (int) (factorial(users.size()) / (factorial(Constants.t + 1) * factorial(users.size() - (Constants.t + 1))));
        int[] indexes = new int[Constants.t + 1];
        for (int i = 0; i < indexes.length - 1; i++) {
            indexes[i] = i;
        }
        indexes[indexes.length - 1] = indexes.length - 2;
        System.out.println("Combinations: " + totalCombinations + " with users: " + users.size() + " t+1=" + (Constants.t + 1));
        for (int k = 0; k < totalCombinations; k++) {

            int cont = indexes.length - 1;
            boolean bucle = false;
            indexes[indexes.length - 1] = indexes[indexes.length - 1] + 1;
            while (indexes[indexes.length - 1] >= (users.size())) {
                bucle = true;
                cont--;
                if (cont < 0) {
                    break;
                }
                indexes[cont] = indexes[cont] + 1;
                for (int i = 1; i + cont < indexes.length; i++) {
                    indexes[cont + i] = indexes[cont + i - 1] + 1;
                }
            }

            ArrayList<Integer> combination = new ArrayList<>();
            for (int i = 0; i < indexes.length; i++) {
                combination.add(indexes[i]);
            }
            combinations.add(combination);
        }
        ArrayList<ArrayList<Integer>> combinations2 = new ArrayList<>();
        for (ArrayList<Integer> combination : combinations) {
            ArrayList<Integer> comb = new ArrayList<>();
            for (Integer i : combination) {
                comb.add(i + 1);
            }
            combinations2.add(comb);
        }/*
        for (ArrayList<Integer> combination : combinations2) {
            System.out.print("[");
            for (Integer i : combination) {
                System.out.print(i + ",");
            }
            System.out.println("]");
        }*/
        Constants.J_COMBINATIONS = combinations2;
    }

    private double factorial(double x) {
        return Gamma.gamma(x + 1);
    }
}
