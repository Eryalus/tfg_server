/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.placement.algorithm;

import com.tfg.server.fileProcessing.ProcessedFile;
import com.tfg.server.topology.Topology;
import java.util.ArrayList;

/**
 *
 * @author Eryalus
 */
public interface PlacementAlgorithm {

    public void runAlgorithm(Topology topology, ArrayList<ProcessedFile> files);

}
