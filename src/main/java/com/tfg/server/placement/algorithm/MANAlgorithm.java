/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.placement.algorithm;

import com.tfg.server.Constants;
import com.tfg.server.fileProcessing.ProcessedFile;
import com.tfg.server.fileProcessing.Subfile;
import com.tfg.server.topology.Topology;
import com.tfg.server.topology.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author Eryalus
 */
public class MANAlgorithm implements PlacementAlgorithm {

    @Override
    public void runAlgorithm(Topology topology, ArrayList<ProcessedFile> files) {
        ArrayList<User> users = new ArrayList<>();
        Collection<User> usersColl = topology.getUsers().values();
        usersColl.forEach((u) -> {
            users.add(u);
        });
        Collections.sort(users);
        for (ProcessedFile pFile : files) {
            int[] indexes = new int[Constants.t];
            for (int i = 0; i < indexes.length; i++) {
                indexes[i] = i;
            }
            for (Subfile subfile : pFile.getSubfiles()) {
                int cont = indexes.length - 1;
                while (indexes[indexes.length - 1] >= users.size()) {
                    cont--;
                    if (cont < 0) {
                        break;
                    }
                    indexes[cont] = indexes[cont] + 1;
                    for (int i = 1; i + cont < indexes.length; i++) {
                        indexes[cont + i] = indexes[cont + i - 1] + 1;
                    }
                }
                for (int i = 0; i < indexes.length; i++) {
                    subfile.getUsers().add(users.get(indexes[i]));
                }
                indexes[indexes.length - 1] = indexes[indexes.length - 1] + 1;
            }
        }
    }

}
