/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.placement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.server.Constants;
import com.tfg.server.fileProcessing.ProcessedFile;
import com.tfg.server.fileProcessing.Subfile;
import com.tfg.server.utils.SocketWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eryalus
 */
public class Populating {

    public static void populate(ArrayList<ProcessedFile> processedFiles) {
        for (ProcessedFile pf : processedFiles) {
            Constants.LOG.info(pf.getFile().getName() + " " + pf.getId());
            for (Subfile sf : pf.getSubfiles()) {
                Constants.LOG.info("Subpart number: " + sf.getFile().getName());
                try {
                    populate(sf, pf.getId());
                } catch (IOException ex) {
                    Constants.LOG.error("IOException", ex);
                    System.exit(-1);
                }
            }
        }
        try {
            Constants.WRITER.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED);
            Constants.WRITER.flush();
        } catch (IOException ex) {
            Constants.LOG.error("IOException", ex);
            System.exit(-1);
        }

    }

    private static void populate(Subfile subfile, int fileID) throws IOException {
        SocketWriter writer = Constants.WRITER;
        writer.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART);
        int[] users = new int[subfile.getUsers().size()];
        for (int i = 0; i < users.length; i++) {
            users[i] = subfile.getUsers().get(i).getId();
        }
        writer.writeString(new ObjectMapper().writeValueAsString(users));
        writer.writeInt32(fileID);
        writer.writeInt32(Integer.parseInt(subfile.getFile().getName()));
        writer.flush();
        writer.writeFile(subfile.getFile());
    }
}
