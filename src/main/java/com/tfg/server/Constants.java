/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server;

import com.tfg.server.topology.Topology;
import com.tfg.server.utils.SocketWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eryalus
 */
public class Constants {

    /*
     * Constants
     */
    public static final String TOPOLOGY_JSON_PATH = "res/topology.json";
    public static final String FILES_JSON_PATH = "res/files.json";
    public static final String SUBFILES_PATH = "subfiles";
    public static final String W_FILES_PATH = "W_Files";
    public static final String SERVER_MULTICAST_IP = "224.0.0.1";
    public static final int SERVER_MULTICAST_PORT = 4446;
    public static final int SERVER_PORT = 2000;

    /*
     * Logger
     */
    public static final Logger LOG = LoggerFactory.getLogger("TFG Server");

    /*
     * Semaphores
     */
    public static final Semaphore PLACEMENT_SEM = new Semaphore(0);
    public static final Semaphore ACCEPTING_REQUEST_SEM = new Semaphore(0);
    public static final Semaphore READY_D_VECTOR_SEM = new Semaphore(0);
    public static final Semaphore D_VECTOR_SEM = new Semaphore(1);

    /*
     * Global vars
     */
    public static int H, K, r, N, M, t, PARTS_FOR_FILE;
    public static long B, MB;
    public static SocketWriter WRITER;
    public static Topology TOPOLOGY;
    public static final HashMap<Integer, Integer> D_VECTOR = new HashMap<>(); //key: userID; value: fileID
    public static ArrayList<ArrayList<Integer>> J_COMBINATIONS = new ArrayList<>();
    public static long SUBFILE_SIZE;

    public static final class MESSAGE_ACTIONS_TYPE {

        public static final int SETUP = 1, PLACEMENT_PART = 2, PLACEMENT_ENDED = 3, PHASE_1_MESSAGE = 4, PHASE_2_MESSAGE = 5, DELIVEY_ENDED = 6;

    }

}
