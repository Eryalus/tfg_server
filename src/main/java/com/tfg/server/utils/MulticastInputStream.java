/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MulticastInputStream extends InputStream implements Runnable {

    private static final int DATAGRAM_PACKET_SIZE = 1024;
    private final LinkedList<Byte> queue;
    private final Semaphore sem;
    private final MulticastSocket socket;
    private long bytesReaded = 0;
    private HashMap<Long, ReadedPackage> UnattendedPackages = new HashMap<>();
    private long lastPackageAdded = -1L;

    public long getBytesReaded() {
        return bytesReaded;
    }

    public MulticastInputStream(MulticastSocket socket) {
        super();
        this.queue = new LinkedList<>();
        sem = new Semaphore(1);
        this.socket = socket;
    }

    private void push(byte b) {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        bytesReaded++;
        queue.addFirst(b);
        sem.release();
    }

    private void push(byte[] bs) {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        bytesReaded++;
        for (Byte b : bs) {
            queue.addFirst(b);
        }
        sem.release();
    }

    private byte pop() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        Byte removeLast = queue.removeLast();
        sem.release();
        return removeLast;
    }

    @Override
    public int read() throws IOException {
        waitFor(1);
        return pop();
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        waitFor(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = pop();
        }
        return bytes.length;
    }

    @Override
    public int available() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        int size = queue.size();
        sem.release();
        return size;
    }

    private void waitFor(int bytes_size) throws IOException {
        while (available() < bytes_size) {
            try {
                synchronized (this) {
                    wait(100);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SocketReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        byte[] secNumber = new byte[8];
        long number;
        byte[] buf = new byte[DATAGRAM_PACKET_SIZE];
        DatagramPacket p = new DatagramPacket(buf, DATAGRAM_PACKET_SIZE);
        while (true) {
            try {
                socket.receive(p);
                ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
                System.arraycopy(p.getData(), 0, secNumber, 0, 8);
                buffer.put(secNumber);
                buffer.flip();
                number = buffer.getLong();
                byte[] temp = new byte[p.getData().length - 8];
                for (int i = 8; i < p.getData().length; i++) {
                    temp[i - 8] = p.getData()[i];
                }
                if (number == lastPackageAdded + 1) {
                    push(temp);
                    lastPackageAdded = number;
                } else {
                    UnattendedPackages.put(number, new ReadedPackage(number, temp));
                }
                boolean checkNext;
                do {
                    checkNext = false;
                    for (Long key : UnattendedPackages.keySet()) {
                        if (key == lastPackageAdded + 1) {
                            push(UnattendedPackages.get(key).ReadedBytes);
                            lastPackageAdded = key;
                            checkNext = true;
                            UnattendedPackages.remove(key);
                            break;
                        }
                    }
                } while (checkNext);
            } catch (IOException ex) {
                Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class ReadedPackage {

        private final byte[] ReadedBytes;
        private final long SecuenceNumber;

        public ReadedPackage(long secuenceNumber, byte[] readedBytes) {
            SecuenceNumber = secuenceNumber;
            ReadedBytes = readedBytes;
        }

        public byte[] getReadedBytes() {
            return ReadedBytes;
        }

        public long getSecuenceNumber() {
            return SecuenceNumber;
        }

    }
}
