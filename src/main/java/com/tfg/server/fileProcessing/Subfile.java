/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.fileProcessing;

import com.tfg.server.topology.User;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class Subfile {

    private File file;
    private ArrayList<User> users = new ArrayList<>();

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
}
