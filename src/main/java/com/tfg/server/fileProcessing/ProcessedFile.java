/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.fileProcessing;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class ProcessedFile {

    private File file;
    private ArrayList<Subfile> subfiles = new ArrayList<>();
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<Subfile> getSubfiles() {
        return subfiles;
    }

    public void setSubfiles(ArrayList<Subfile> subfiles) {
        this.subfiles = subfiles;
    }

}
