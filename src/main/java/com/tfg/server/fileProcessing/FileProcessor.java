/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.fileProcessing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.server.Constants;
import com.tfg.server.fileProcessing.JSON.FileJSON;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.math3.special.Gamma;

/**
 *
 * @author eryalus
 */
public class FileProcessor {

    public ArrayList<ProcessedFile> processFiles() {
        Constants.LOG.info("Reading file list from JSON file...");
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(Constants.FILES_JSON_PATH);
        FileJSON readedFiles;
        try {
            readedFiles = mapper.readValue(file, FileJSON.class);
        } catch (IOException ex) {
            Constants.LOG.error("Can not read file list from JSON", ex);
            return null;
        }
        Constants.LOG.info("JSON Readed.");
        Constants.LOG.info("Processing file list...");
        ArrayList<File> files = new ArrayList<>();
        long maxFileSize = 0L;
        for (String f : readedFiles.getFiles()) {
            File temporaryFile = new File(f);
            if (temporaryFile.exists()) {
                if (temporaryFile.isFile()) {
                    maxFileSize = Math.max(maxFileSize, temporaryFile.length());
                    files.add(temporaryFile);
                } else {
                    Constants.LOG.error("File \"" + f + "\" is a directory. Aborting file processing...");
                    return null;
                }
            } else {
                Constants.LOG.error("File \"" + f + "\" does not exist. Aborting file processing...");
                return null;
            }
        }
        Constants.B = maxFileSize;
        Constants.N = readedFiles.getFiles().length;
        Constants.LOG.info("Files processed. B=" + maxFileSize + " N=" + Constants.N);
        Constants.LOG.info("Partitioning files...");
        Constants.LOG.info("Calculate partition size...");
        long numberOfParts = calculateNumberOfParts();
        Constants.PARTS_FOR_FILE = (int) numberOfParts;
        Constants.LOG.info("Total parts: " + numberOfParts);
        long subfileSize = (long) Math.ceil(((double) Constants.B) / numberOfParts);
        Constants.SUBFILE_SIZE = subfileSize;
        Constants.LOG.info("Subfile Size:" + subfileSize);
        Constants.LOG.info("Writing files to disk...");
        byte[] block = new byte[(int) subfileSize];
        File base_path = new File(Constants.SUBFILES_PATH);
        base_path.mkdirs();
        ArrayList<ProcessedFile> processedFiles = new ArrayList<>();
        int processedFileIDCounter = 0;
        for (File f : files) {
            ProcessedFile processedFile = new ProcessedFile();
            processedFile.setFile(f);
            processedFile.setId(processedFileIDCounter++);
            try {
                File subfileParentDirectory = new File(Constants.SUBFILES_PATH + "/" + f.getName());
                subfileParentDirectory.mkdirs();
                FileInputStream fis = new FileInputStream(f);
                int index = 0;
                int readed;
                while ((readed = fis.read(block)) != -1) {
                    File subfileFile = new File(Constants.SUBFILES_PATH + "/" + f.getName() + "/" + index++);
                    Subfile subfile = new Subfile();
                    subfile.setFile(subfileFile);
                    processedFile.getSubfiles().add(subfile);
                    if (subfileFile.exists() && subfileFile.length() == subfileSize) {
                        Constants.LOG.info("Skipping " + subfileFile.getAbsolutePath());
                        continue;
                    }

                    FileOutputStream fos = new FileOutputStream(subfileFile);
                    for (int i = readed; i < subfileSize; i++) {
                        block[i] = 0x0;
                    }
                    fos.write(block);
                    fos.close();
                }
                for (int i = 0; i < subfileSize; i++) {
                    block[i] = 0x0;
                }
                while (index < numberOfParts) {
                    File subfileFile = new File(Constants.SUBFILES_PATH + "/" + f.getName() + "/" + index++);
                    Subfile subfile = new Subfile();
                    subfile.setFile(subfileFile);
                    processedFile.getSubfiles().add(subfile);
                    if (subfileFile.exists() && subfileFile.length() == subfileSize) {
                        Constants.LOG.info("Skipping " + subfileFile.getAbsolutePath());
                        continue;
                    }
                    FileOutputStream fos = new FileOutputStream(subfileFile);
                    fos.write(block);
                    fos.close();
                }

                fis.close();
            } catch (FileNotFoundException ex) {
                Constants.LOG.error("File \"" + f + "\" can not be found. Aborting file processing...");
            } catch (IOException ex) {
                Constants.LOG.error("File \"" + f + "\" can not be processed. Aborting file processing...", ex);
            }
            processedFiles.add(processedFile);
        }
        Constants.LOG.info("Files written");
        Constants.LOG.info("Files partitioned.");
        return processedFiles;
    }

    private long calculateNumberOfParts() {
        //Summary in page 2
        double M = ((double) Constants.MB) / Constants.B;
        Constants.M = (int) Math.ceil(M);
        double temp = ((double) (M * Constants.K) / Constants.N);
        double t = Math.min(Constants.K, temp);
        Constants.t = (int) Math.ceil(t);
        double KFactorial = factorial(Constants.K);
        double tFactorial = factorial(t);
        double K_tFactorial = factorial(Constants.K - t);
        double numberOfParts = KFactorial / (tFactorial * K_tFactorial);
        return (long) Math.ceil(numberOfParts);
    }

    private double factorial(double x) {
        return Gamma.gamma(x + 1);
    }

}
