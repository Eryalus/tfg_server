/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server.users.management;

import com.tfg.server.Constants;
import static com.tfg.server.Constants.LOG;
import com.tfg.server.topology.Relay;
import com.tfg.server.topology.User;
import com.tfg.server.utils.SocketRW;
import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Eryalus
 */
public class UnicastConnectionManager implements Runnable {

    SocketRW rw;
    User user;
    int id, totalUsers;
    String summary;
    private static int SUMMARY_ACK_COUNT = 0;
    private static Semaphore SUMMARY_ACK_COUNT_SEM = new Semaphore(1);

    public UnicastConnectionManager(SocketRW rw, User user, int id, String summary) {
        this.rw = rw;
        this.user = user;
        this.id = id;
        this.summary = summary;
        this.totalUsers = Constants.TOPOLOGY.getUsers().keySet().size();
    }

    @Override
    public void run() {
        try {
            String multicastLinks = "";
            boolean first = true;
            for (Relay r : user.getConnectedRelays()) {
                if (first) {
                    first = false;
                } else {
                    multicastLinks += ",";
                }
                multicastLinks += r.getIp() + ":" + r.getPort();
            }
            rw.getWriter().writeString(multicastLinks);
            rw.getWriter().flush();
            LOG.info("Waiting for the ack of " + id);
            rw.getReader().readLong();
            LOG.info("ACK received from " + id);
            rw.getWriter().writeString(summary);
            rw.getWriter().flush();
            rw.getReader().readLong();
            SUMMARY_ACK_COUNT_SEM.acquire();
            SUMMARY_ACK_COUNT++;
            if (SUMMARY_ACK_COUNT == totalUsers) {
                LOG.info("All users are ready for placement.");
                Constants.PLACEMENT_SEM.release();
            }
            SUMMARY_ACK_COUNT_SEM.release();
            Constants.ACCEPTING_REQUEST_SEM.acquire();
            while (true) {
                int fileID = rw.getReader().readInt32();
                Constants.LOG.info("User " + this.id + " requests file " + fileID);
                boolean complete = false;
                Constants.D_VECTOR_SEM.acquire();
                Constants.D_VECTOR.put(id, fileID);
                if (Constants.D_VECTOR.keySet().size() == Constants.TOPOLOGY.getUsers().keySet().size()) {
                    //D_VECTOR complete
                    complete = true;
                }
                Constants.D_VECTOR_SEM.release();
                if (complete) {
                    Constants.READY_D_VECTOR_SEM.release();
                }
            }
        } catch (IOException ex) {
            LOG.error("IOException", ex);
        } catch (InterruptedException ex) {
            LOG.error("Concurrency error. Exiting.", ex);
            System.exit(-1);
        }
    }

}
