/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.tfg.server.Constants.TOPOLOGY;
import com.tfg.server.delivery.Phase1KnownData;
import com.tfg.server.delivery.Utils;
import com.tfg.server.delivery.algorithm.phase1.GeneralAlgorithmPhase1;
import com.tfg.server.delivery.algorithm.phase1.Phase1Sender;
import com.tfg.server.delivery.algorithm.phase1.W_File;
import com.tfg.server.delivery.algorithm.phase2.GeneralAlgorithmPhase2;
import com.tfg.server.delivery.algorithm.phase2.PToSend;
import com.tfg.server.delivery.algorithm.phase2.Phase2Sender;
import com.tfg.server.fileProcessing.FileProcessor;
import com.tfg.server.fileProcessing.ProcessedFile;
import com.tfg.server.files.FilesSummary;
import com.tfg.server.placement.Populating;
import com.tfg.server.placement.algorithm.MANAlgorithm;
import com.tfg.server.topology.Topology;
import com.tfg.server.topology.TopologyManager;
import com.tfg.server.topology.User;
import com.tfg.server.users.management.UnicastConnectionManager;
import com.tfg.server.utils.MulticastOutputStream;
import com.tfg.server.utils.SocketRW;
import com.tfg.server.utils.SocketReader;
import com.tfg.server.utils.SocketWriter;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import org.slf4j.Logger;

/**
 *
 * @author eryalus
 */
public class Main {

    private static final Logger LOG = Constants.LOG;
    private static final MANAlgorithm PLACEMENT_MAN_ALGORITHM = new MANAlgorithm();
    private MulticastOutputStream MOS;
    private ArrayList<ProcessedFile> PROCESSED_FILES;

    private final HashMap<Integer, SocketRW> USERS_SOCKETS = new HashMap<>();
    private ServerSocket serverSocket = null;

    public static void main(String[] args) throws InterruptedException {
        Main main = new Main();
        main.start();
    }

    public void start() throws InterruptedException {
        LOG.info("[ SETUP ]");
        LOG.info("Setting up...");
        setup();
        LOG.info("Setup done.");
        LOG.info("Waiting for all users to connect...");
        setupUsers();
        LOG.info("All users connected.");
        LOG.info("Waiting for finishing the users setup...");
        try {
            Constants.PLACEMENT_SEM.acquire();
        } catch (InterruptedException ex) {
            LOG.error("Concurrency error. Exiting...", ex);
            System.exit(-1);
        }
        LOG.info("Users setup ended.");
        LOG.info("[ PLACEMENT ]");
        LOG.info("Start populating users caches...");
        Populating.populate(PROCESSED_FILES);
        LOG.info("Populating ended.");
        Constants.ACCEPTING_REQUEST_SEM.release(Constants.TOPOLOGY.getUsers().keySet().size());
        LOG.info("Added bytes: " + MOS.getBytesAdded() + " Sended byted: " + MOS.getBytesSended());
        while (true) {
            LOG.info("Waiting for the requests from the users...");
            HashMap<Integer, Integer> D_TEMP = new HashMap<>();
            Constants.READY_D_VECTOR_SEM.acquire();
            Constants.D_VECTOR_SEM.acquire();
            for (int user : Constants.D_VECTOR.keySet()) {
                D_TEMP.put(user, Constants.D_VECTOR.get(user));
            }
            for (int user : D_TEMP.keySet()) {
                Constants.D_VECTOR.remove(user);
            }
            Constants.D_VECTOR_SEM.release();
            try {
                LOG.info("[ DELIVERY - PHASE 1 ]");
                GeneralAlgorithmPhase1.GeneralAlgorithmPhase1Result resultPhase1 = new GeneralAlgorithmPhase1().run(PROCESSED_FILES, D_TEMP);
                //send all files
                Phase1Sender senderPhase1 = new Phase1Sender();
                HashMap<W_File, Phase1KnownData> knownData = senderPhase1.send(resultPhase1);
                LOG.info("[ DELIVERY - PHASE 2 ]");
                ArrayList<PToSend> resultPhase2 = new GeneralAlgorithmPhase2().run(resultPhase1, knownData);
                Phase2Sender senderPhase2 = new Phase2Sender();
                senderPhase2.send(resultPhase2);
                LOG.info("[ DELIVERY - ENDED ]");
                Constants.WRITER.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.DELIVEY_ENDED);
                Constants.WRITER.flush();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void setup() {
        LOG.info("Reading topology...");
        TopologyManager topologyManager = new TopologyManager();
        Topology currentTopology = topologyManager.readTopology();
        if (currentTopology == null) {
            System.exit(-1);
        }
        LOG.info("Topology readed.");
        LOG.info("Calculate params...");
        Constants.H = currentTopology.getRelays().size();
        Constants.K = currentTopology.getUsers().keySet().size();
        int r = 0;
        long MB = Long.MAX_VALUE;
        for (User u : currentTopology.getUsers().values()) {
            r = Math.max(r, u.getConnectedRelays().size());
            MB = Math.min(MB, u.getCacheSize());
        }
        Constants.MB = MB;
        Constants.r = r;
        LOG.info("Calulated. MB=" + Constants.MB + " K=" + Constants.K + " H=" + Constants.H + " r=" + Constants.r);
        FileProcessor fileProcessor = new FileProcessor();
        ArrayList<ProcessedFile> processedFiles = fileProcessor.processFiles();
        LOG.info("Assigning users to each subfile...");
        PLACEMENT_MAN_ALGORITHM.runAlgorithm(currentTopology, processedFiles);
        Constants.TOPOLOGY = currentTopology;
        PROCESSED_FILES = processedFiles;
        LOG.info("Assigment ended.");
        LOG.info("Calculating J with |J | = t + 1 = " + (Constants.t + 1) + "...");
        new Utils().calculateJ();
        String temp = "";
        for (ArrayList<Integer> combination : Constants.J_COMBINATIONS) {
            for (Integer element : combination) {
                temp += element;
            }
            temp += " ";
        }
        temp = temp.trim();
        LOG.info("J calculated.");
        LOG.info("J: " + temp);
        LOG.info("Creating Multicast Writer...");
        try {
            DatagramSocket socket = new DatagramSocket();
            MOS = new MulticastOutputStream(socket, Constants.SERVER_MULTICAST_IP);
            Constants.WRITER = new SocketWriter(MOS);
        } catch (IOException ex) {
            LOG.error("Can not create Multicast Writer.", ex);
            System.exit(-1);
        }
        LOG.info("Multicast Writer created.");
        LOG.info("Creating Unicast ServerSocket...");

        try {
            serverSocket = new ServerSocket(Constants.SERVER_PORT);
        } catch (IOException ex) {
            LOG.error("Can not create Unicast ServerSocket", ex);
            System.exit(-1);
        }
        LOG.info("Unicast ServerSocket created.");
    }

    private void setupUsers() {
        LOG.info("Creating collections summary...");
        ArrayList<String> fileNames = new ArrayList<>();
        int[] ids = new int[PROCESSED_FILES.size()];
        int count = 0;
        for (ProcessedFile pf : PROCESSED_FILES) {
            fileNames.add(pf.getFile().getName());
            ids[count++] = pf.getId();
        }
        FilesSummary filesSummary = new FilesSummary(fileNames, ids, Constants.B, Constants.PARTS_FOR_FILE);
        String summary = null;
        try {
            summary = new ObjectMapper().writeValueAsString(filesSummary);
        } catch (JsonProcessingException ex) {
            LOG.error("Error parsing filesSummary", ex);
            System.exit(-1);
        }
        LOG.info("Collection summary created.");
        LOG.info("Opening connections for all users...");
        boolean ended = false;
        while (!ended) {
            try {
                Socket accept = serverSocket.accept();
                LOG.info("Connection opened from " + accept.getInetAddress().getHostAddress() + ":" + accept.getPort());
                SocketReader reader = new SocketReader(accept.getInputStream());
                SocketWriter writer = new SocketWriter(accept.getOutputStream());
                SocketRW rw = new SocketRW(reader, writer);
                rw.setSocket(accept);
                try {
                    int id = rw.getReader().readInt32();
                    LOG.info("Readed id " + id + " from IP " + rw.getSocket().getInetAddress().getHostAddress());
                    Collection<User> values = TOPOLOGY.getUsers().values();
                    for (User u : values) {
                        if (u.getId() == id) {
                            LOG.info("Addind user " + id + " on " + accept.getInetAddress().getHostAddress() + ":" + accept.getPort());
                            //start manager thread
                            new Thread(new UnicastConnectionManager(rw, u, id, summary)).start();
                            USERS_SOCKETS.put(id, rw);
                            break;
                        }
                    }
                    if (USERS_SOCKETS.keySet().size() == TOPOLOGY.getUsers().keySet().size()) {
                        //all users connected
                        LOG.info("All users are online.");
                        ended = true;
                    }

                } catch (IOException ex) {
                    LOG.error("Can not read data from " + rw.getSocket().getInetAddress().getHostAddress(), ex);
                }
            } catch (IOException ex) {
                LOG.error("Can not open connection.", ex);
            }
        }
    }
}
